const categories = [
    {
      name: 'Tests par catégorie',
      links: [
        {
          name: 'RPG',
          ref: '#RPG',
        },
        {
          name: 'MMO',
          ref: '#MMO',
        },
        {
          name: 'FPS/TPS',
          ref: '#FPS',
        },
        {
          name: 'RTS',
          ref: '#RTS',
        },
      ],
    },
    {
      name: 'Nous contacter',
      links: [
        {
          name: 'Email',
          ref: '#',
        },
        {
          name: 'Flux RSS',
          ref: '#',
        },
        {
          name: 'Newsletter',
          ref: '#',
        },
      ],
    },
    {
      name: 'Assistance',
      links: [
        {
          name: 'Mentions légales',
          ref: '#',
        },
        {
          name: 'Support',
          ref: '#',
        },
        {
          name: 'Charte de confidentialité',
          ref: '#',
        },
      ],
    },
  ];
  
  const footer = {
    categories,
  };
  
  export default footer;
  