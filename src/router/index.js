import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/pages/home/Index.vue';
import Games from '@/pages/games/Index.vue';
import CreateGame from '@/pages/create_game/Index.vue';
import Register from '@/pages/register/Index.vue';
import Login from '@/pages/login/Index.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/games/:id',
    name: 'games',
    component: Games,
  },
  {
    path: '/create-game-test',
    name: 'create-game-test',
    component: CreateGame,
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

export default router;
